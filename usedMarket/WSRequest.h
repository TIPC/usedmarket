//
//  WSRequest.h
//  altibbiApp
//
//  Created by TIPC on 7/26/13.
//  Copyright (c) 2013 altibbiApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSRequest : NSObject

+(BOOL)checkNetworkStatus;
-(void)sendRequest:(NSString *)action
 completionHandler:(void (^)(NSDictionary *recivedData))completeHandler;

@property (strong, nonatomic) NSDictionary *sendData;

@end
