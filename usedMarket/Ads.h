//
//  Ads.h
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Images, Specification;

@interface Ads : NSManagedObject

@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * categoryId;
@property (nonatomic, retain) NSString * adsId;
@property (nonatomic, retain) NSString * adsTitle;
@property (nonatomic, retain) NSDate * publishDate;
@property (nonatomic, retain) NSNumber * adsType;
@property (nonatomic, retain) NSString * descrp;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * priceType;
@property (nonatomic, retain) NSNumber * countryId;
@property (nonatomic, retain) NSString * city;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * ownerId;
@property (nonatomic, retain) NSString * ownerName;
@property (nonatomic, retain) NSString * ownerEmail;
@property (nonatomic, retain) NSString * ownerMobile;
@property (nonatomic, retain) NSSet *imageList;
@property (nonatomic, retain) NSSet *specifications;
-(CTAdsModel *)getAdsModel;
@end

@interface Ads (CoreDataGeneratedAccessors)

- (void)addImageListObject:(Images *)value;
- (void)removeImageListObject:(Images *)value;
- (void)addImageList:(NSSet *)values;
- (void)removeImageList:(NSSet *)values;

- (void)addSpecificationsObject:(Specification *)value;
- (void)removeSpecificationsObject:(Specification *)value;
- (void)addSpecifications:(NSSet *)values;
- (void)removeSpecifications:(NSSet *)values;

@end
