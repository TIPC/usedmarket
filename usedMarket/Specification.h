//
//  Specification.h
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ads;

@interface Specification : NSManagedObject

@property (nonatomic, retain) NSString * pId;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) Ads *adsItem;

@end
