//
//  CTAdsListTVC.m
//  usedMarket
//
//  Created by TIPC on 6/18/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTAdsListTVC.h"
#import "CTSuperCell.h"
#import "RTSpinKitView.h"
#import "CTAdsDetailsTVC.h"

@implementation CTAdsListTVC
{
    NSMutableArray *tableData;
    BOOL isGrid;
    UIButton *sideMenuBtn;
    RTSpinKitView *loader;
    UIButton *reloadButton;
    UIButton *emptyButton;

    BOOL addMore;
    BOOL isEmpty;
    BOOL isLoading;

}
@synthesize selectedIndex;
-(void)viewDidLoad
{
    [super viewDidLoad];
    isEmpty = NO;
    isLoading = NO;
    self.navigationItem.title = CTShareInstance.categoryList[CTShareInstance.categoryIndex][@"name_ar"];
    tableData = [NSMutableArray new];
    isGrid = YES;
    selectedIndex = -1;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CTiPhoneGridCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"gridCell"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CTiPhoneListCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"listCell"];
    
    

    UIButton *button;
    UIBarButtonItem *sideMenu, *switchMode;
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"sideMenuIcon"] forState:UIControlStateNormal];
    button.bounds = CGRectMake(0,0,button.imageView.image.size.width, button.imageView.image.size.height);
    [button addTarget:self action:@selector(presentRightMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    sideMenu = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    
    sideMenuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sideMenuBtn setImage:[UIImage imageNamed:@"gridMenuIcon"] forState:UIControlStateNormal];
    sideMenuBtn.bounds = CGRectMake(0,0,
                                    sideMenuBtn.imageView.image.size.width,
                                    sideMenuBtn.imageView.image.size.height);
    [sideMenuBtn addTarget:self action:@selector(switchViewMode)
          forControlEvents:UIControlEventTouchUpInside];
    switchMode = [[UIBarButtonItem alloc] initWithCustomView:sideMenuBtn];
    

    self.navigationItem.rightBarButtonItems = @[sideMenu, switchMode];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    loader = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:CTNaviBlueColor];
    loader.hidesWhenStopped = YES;
    CGRect loaderFrame = loader.frame;
    loaderFrame.origin.x = (self.view.frame.size.width - loaderFrame.size.width)/2;
    loaderFrame.origin.y = (self.view.frame.size.height - loaderFrame.size.height)/2;
    loaderFrame.origin.y -= 65;
    loader.frame = loaderFrame;
    [loader stopAnimating];
    [self.view addSubview:loader];
    [self getDataFromWebService];
    
    reloadButton = [UIButton new];
    emptyButton = [UIButton new];
    CGFloat centerWidth = 240;
    CGFloat centerHeight = 240;

    reloadButton.frame = CGRectMake((self.view.frame.size.width-centerWidth)/2,
                                    ((self.view.frame.size.height-centerHeight)/2)-30,
                                    centerWidth,
                                    centerHeight);
    emptyButton.frame = reloadButton.frame;
    [reloadButton setTitle:@"Reload Data" forState:UIControlStateNormal];
    [emptyButton setTitle:@"Add Offer\n+" forState:UIControlStateNormal];
    reloadButton.hidden = YES;
    emptyButton.hidden = YES;
    
    [reloadButton addTarget:self
                     action:@selector(getDataFromWebService)
           forControlEvents:UIControlEventTouchUpInside];
    [emptyButton addTarget:self
                     action:@selector(addOffer)
           forControlEvents:UIControlEventTouchUpInside];
    
    reloadButton.backgroundColor = [UIColor redColor];
    emptyButton.backgroundColor = [UIColor greenColor];
    [self.view addSubview:emptyButton];
    [self.view addSubview:reloadButton];

}
-(void)addOffer
{
    [self.sideMenuViewController setContentViewController:CTShareInstance.appViews[@"addOffer"]
                                                 animated:YES];
}
-(void)getDataFromWebService
{
    reloadButton.hidden = YES;
    emptyButton.hidden = YES;
    isLoading = (tableData.count > 0);
    
    [self.tableView reloadData];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [loader startAnimating];
    if (isLoading)
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:tableData.count inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    CTShareInstance.request.sendData = @{@"category_id" : CTShareInstance.selectedCatId,
                                         @"offset" : @(tableData.count),
                                         @"country_id" : CTShareInstance.countryList[CTShareInstance.countryIndex][@"id"]
                                         };

    [CTShareInstance.request sendRequest:@"items/get_items"
                       completionHandler:^(NSDictionary *recivedData)
    {
        [loader stopAnimating];
        isLoading = NO;
        if ([recivedData[@"status"] boolValue] ||
            [recivedData[@"status"] isEqualToString:@"success"])
        {
            for (NSDictionary *tempAdsData in recivedData[@"data"])
                [tableData addObject:[CTShareInstance prepareItem:tempAdsData]];
            emptyButton.hidden = (tableData.count != 0);
            addMore = ([recivedData[@"data"] count] >= 10);
        }
        else
            reloadButton.hidden = NO;
        
        if (!emptyButton.hidden && !reloadButton.hidden)
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        [self.tableView reloadData];
        
    }];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableData.count <= indexPath.row)
        return 44;
    if (isGrid)
        return (selectedIndex == indexPath.row) ? 290 : 195;
    else
        return (selectedIndex == indexPath.row) ? 180 : 105;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (isLoading)?tableData.count+1 : tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (tableData.count <= indexPath.row)
    {
        UITableViewCell *cell = [UITableViewCell new];
        RTSpinKitView *cellLoader = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:CTNaviBlueColor];
        cellLoader.hidesWhenStopped = YES;
        CGRect loaderFrame = cellLoader.frame;
        loaderFrame.origin.x = (self.view.frame.size.width - loaderFrame.size.width)/2;
        loaderFrame.origin.y = (44 - loaderFrame.size.height)/2;
        cellLoader.frame = loaderFrame;
        [cellLoader startAnimating];
        [cell.contentView addSubview:cellLoader];
        return cell;
        
    }
    else
    {
        NSString *cellIdentifier = (isGrid) ? @"gridCell" : @"listCell";
        CTSuperCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.parentVC = self;
        cell.tag = indexPath.row;
        cell.item = tableData[indexPath.row];
        return cell;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < tableData.count)
    {
        CTAdsDetailsTVC *dstVC = [CTAdsDetailsTVC new];
        dstVC.item = tableData[indexPath.row];
            self.navigationItem.backBarButtonItem =     [[UIBarButtonItem alloc] initWithTitle:@""
                                                                                     style:UIBarButtonItemStyleBordered
                                                                                    target:nil
                                                                                    action:nil];
        
        [self.navigationController pushViewController:dstVC animated:YES];
    }

}

-(IBAction)switchViewMode
{
    isGrid = !isGrid;
    NSString *iconName = (isGrid)? @"gridMenuIcon" : @"listMenuIcon";
    [sideMenuBtn setImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];

    [self.tableView reloadData];
    
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == self.tableView)
    {
        NSInteger scrollOffset = (scrollView.contentOffset.y + self.tableView.frame.size.height);
        NSInteger contentHeight = self.tableView.contentSize.height - 10;
        if ((scrollOffset >= contentHeight) && addMore)
            [self getDataFromWebService];
    }
    
}

@end
