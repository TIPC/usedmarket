//
//  CTAppDelegate.h
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTAppDelegate : UIResponder <UIApplicationDelegate, RESideMenuDelegate>

@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;
@property (strong, nonatomic) UIWindow *window;

@end