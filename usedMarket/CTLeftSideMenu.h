//
//  CTLeftSideMenu.h
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTLeftSideMenu : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) UITableView *tableView;

@end
