//
//  CTSpecificationItem.h
//  usedMarket
//
//  Created by TIPC on 6/22/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTSpecificationItem : NSObject
@property (strong,nonatomic) NSString *label;
@property (strong,nonatomic) NSString *specId;
@property (nonatomic) NSInteger keyboardType;
@property (strong,nonatomic) NSString *value;
+(CTSpecificationItem *)specificationWithLabel:(NSString *)lbl forId:(NSString *)specID;
+(CTSpecificationItem *)specificationFromSpec:(CTSpecificationItem *)oldSpec;
+(CTSpecificationItem *)specificationWithLabel:(NSString *)lbl forId:(NSString *)specID keyboard:(NSInteger)keyType;
@end
