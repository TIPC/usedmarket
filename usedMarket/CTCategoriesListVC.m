//
//  CTCategoriesListVC.m
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTCategoriesListVC.h"
#import "CTCountryListTVC.h"
#import "CTAdsListTVC.h"
@implementation CTCategoriesListVC
{
    UIButton *countryList;
    UIImageView *flagImg;
    UILabel *countryLbl;
    CGFloat icarouselHeight;
    UIPageControl *pageController;
    NSArray *categoryList;
}
@synthesize carousel;

-(void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = CTBackgroudColor;
    categoryList = CTShareInstance.categoryList;
    icarouselHeight = ([UIScreen mainScreen].bounds.size.height == 480.0)?300 : 350;
    titleView = @"السوق";

    UIBarButtonItem *sideMenu = [UIBarButtonItem new];
    sideMenu.image = [UIImage imageNamed:@"menu-icon"];
    [sideMenu setAction:@selector(presentRightMenuViewController:)];
    self.navigationItem.rightBarButtonItem = sideMenu;
    
    carousel = [iCarousel new];
    carousel.type = iCarouselTypeRotary;
    CGFloat naviHeight = 65;
    CGFloat frameTop = ((self.view.frame.size.height - naviHeight) - icarouselHeight)/2;
    frameTop += 85;
    carousel.frame = CGRectMake(0, frameTop, 320, icarouselHeight);
    carousel.delegate = self;
    carousel.dataSource = self;
    [self.view addSubview:carousel];
    
    
    pageController = [UIPageControl new];
    pageController.backgroundColor = [UIColor clearColor];
    pageController.numberOfPages = categoryList.count;
    CGRect pageFrame = pageController.frame;
    pageFrame.size.width = self.view.frame.size.width;
    pageFrame.origin.x = 0;
    pageFrame.origin.y = carousel.frame.size.height + carousel.frame.origin.y+20;
    pageController.frame = pageFrame;
    [self.view addSubview:pageController];
    
    
    
    
    frameTop = (frameTop - (naviHeight + 40))/2;
    frameTop += naviHeight;
    
    countryList = [UIButton new];
    [countryList addTarget:self
                    action:@selector(selectCountry)
          forControlEvents:UIControlEventTouchUpInside];
    
    countryList.frame = CGRectMake(0, 65, 320, 40);
    countryList.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:countryList];
    
    flagImg = [UIImageView new];
    flagImg.frame = CGRectMake(280.0, 4.0, 32.0, 32.0);

    countryLbl = [UILabel new];
    countryLbl.backgroundColor = [UIColor clearColor];
    countryLbl.frame = CGRectMake(30.0, 2.0, 240.0, 40.0);
    countryLbl.textAlignment = NSTextAlignmentRight;
    countryLbl.textColor = [UIColor darkGrayColor];
    countryLbl.font = [UIFont fontWithName:CTGeneralFont size:13.0];
    
    
    UIImageView *arrowIcon = [UIImageView new];
    arrowIcon.frame = CGRectMake(12.0, 12.0, 17.0, 17.0);
    arrowIcon.image = [UIImage imageNamed:@"arrow"];

    [countryList addSubview:countryLbl];
    [countryList addSubview:flagImg];
    [countryList addSubview:arrowIcon];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    flagImg.image = [UIImage imageNamed:CTShareInstance.countryList[CTShareInstance.countryIndex][@"icon"]];
    countryLbl.text = CTShareInstance.countryList[CTShareInstance.countryIndex][@"name"];

}

-(void)selectCountry
{
    CTCountryListTVC *dstVC = [CTCountryListTVC new];
    [self.navigationController pushViewController:dstVC animated:YES];
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return categoryList.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{

        
    UIView *catogeryCard = [UIView new];
    catogeryCard.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
    catogeryCard.layer.cornerRadius = 11.0;
    catogeryCard.layer.masksToBounds = YES;
    catogeryCard.layer.borderColor = [UIColor lightGrayColor].CGColor;
    catogeryCard.layer.borderWidth = 1.0;
    catogeryCard.frame = CGRectMake(0, 0, 220*(icarouselHeight/300), icarouselHeight);
    
    UIImageView *cardImg = [UIImageView new];
    cardImg.image = [UIImage imageNamed:categoryList[index][@"img"]];
    cardImg.frame = CGRectMake((220-icarouselHeight)/2, 0, icarouselHeight, icarouselHeight);

    UILabel *cardLbl = [UILabel new];
    cardLbl.backgroundColor = [UIColor clearColor];
    cardLbl.text = categoryList[index][@"name_ar"];
    cardLbl.frame = CGRectMake(0, icarouselHeight - 70, catogeryCard.frame.size.width, 40);
    cardLbl.textColor = [UIColor darkGrayColor];
    cardLbl.font = [UIFont fontWithName:CTGeneralFont size:19.0];
    cardLbl.textAlignment = NSTextAlignmentCenter;
    cardLbl.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    
    [catogeryCard addSubview:cardImg];
    [catogeryCard addSubview:cardLbl];
    return catogeryCard;
}



- (BOOL)carousel:(iCarousel *)_carousel shouldSelectItemAtIndex:(NSInteger)index
{

    return YES;
}

- (void)carousel:(iCarousel *)_carousel didSelectItemAtIndex:(NSInteger)index
{
    CTShareInstance.categoryIndex = carousel.currentItemIndex;
    [self.navigationController pushViewController:[CTAdsListTVC new] animated:YES];
}

#pragma mark -
#pragma mark Button tap event

- (void)buttonTapped:(UIButton *)sender
{
    [[[UIAlertView alloc] initWithTitle:@"Button Tapped"
                                message:[NSString stringWithFormat:@"You tapped button number %li", (long)[carousel indexOfItemView:sender]]
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
/*

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionWrap) return YES;
    if (option == iCarouselOptionOffsetMultiplier) return 2*value; // Speed
    if (option == iCarouselOptionSpacing) return 0.27;
    return value;
}
 */
-(CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionSpacing:
            return value * 1.2f;
        case iCarouselOptionWrap:
            return YES;
        case iCarouselOptionFadeMin:
            return -0.2;
        case iCarouselOptionFadeMax:
            return 0.2;
        case iCarouselOptionFadeRange:
            return 2.0;
        default:
            return value;
    }
}




- (void)carouselWillBeginScrollingAnimation:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselDidEndScrollingAnimation:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselDidScroll:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselWillBeginDragging:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselDidEndDragging:(iCarousel *)_carousel willDecelerate:(BOOL)decelerate
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselWillBeginDecelerating:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}
- (void)carouselDidEndDecelerating:(iCarousel *)_carousel
{
    pageController.currentPage = _carousel.currentItemIndex;
}

@end
