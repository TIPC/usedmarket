//
//  WSRequest.m
//  altibbiApp
//
//  Created by TIPC on 7/26/13.
//  Copyright (c) 2013 altibbiApp. All rights reserved.
//

#import "WSRequest.h"
#import "Reachability.h"

@implementation WSRequest

@synthesize sendData;

+(BOOL)checkNetworkStatus
{
    Reachability * internetReachable = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    return !(internetStatus == NotReachable);
}



-(void)sendRequest:(NSString *)action
 completionHandler:(void (^)(NSDictionary *recivedData))completeHandler;
{
    NSLog(@"WSRequest : %@",@"sendHiddenRequest Start");
    if ([WSRequest checkNetworkStatus] == YES)
    {
        NSMutableString *parameters = [NSMutableString new];
        for (NSString *key in self.sendData.allKeys)
            [parameters appendFormat:@"%@=%@&",key,self.sendData[key]];
        NSString *sendParameterStr = [parameters substringToIndex:parameters.length-1];
        NSLog(@"%@",sendParameterStr);

        NSString *targetURL = [@"http://localhost/~tipc/souq/index.php/api/" stringByAppendingString:action];

        NSMutableURLRequest *request = [NSMutableURLRequest new];
        [request setTimeoutInterval:15];
        [request setURL:[NSURL URLWithString:targetURL]];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setValue:@(sendParameterStr.length).stringValue forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:[sendParameterStr dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse* response, NSData* data, NSError* connectionError)
         {
            NSDictionary *recivedData;
             if (connectionError || data == nil)
                 recivedData = @{@"success":@"0", @"msg" : @"حدث خطأ أثناء الإتصال"};
             else
                 recivedData = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
             if (recivedData == nil)
             {
                 recivedData = @{@"success":@"0", @"msg" : @"حدث خطأ أثناء الإتصال"};
                 NSLog(@"WSData : \n%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
             }else
                 NSLog(@"WSData : \n%@",recivedData);
             completeHandler(recivedData);
             
         }];
        NSLog(@"WSRequest : %@",@"sendHiddenRequest Backrground End");

    }
    else
        completeHandler(@{@"success":@"0", @"msg" : @"حدث خطأ أثناء الإتصال"});
}


@end
