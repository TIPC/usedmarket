//
//  CTCountryListTVC.m
//  usedMarket
//
//  Created by TIPC on 6/16/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTCountryListTVC.h"

@interface CTCountryListTVC ()

@end

@implementation CTCountryListTVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"إختر الدولة";
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return CTShareInstance.countryList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    
    UIImageView *flagImg = [UIImageView new];
    flagImg.frame = CGRectMake(270.0, 4.0, 32.0, 32.0);
    flagImg.image = [UIImage imageNamed:CTShareInstance.countryList[indexPath.row][@"icon"]];
    
    UILabel *countryLbl = [UILabel new];
    countryLbl.backgroundColor = [UIColor clearColor];
    countryLbl.text = CTShareInstance.countryList[indexPath.row][@"name"];
    countryLbl.frame = CGRectMake(30.0, 2.0, 230.0, 40.0);
    countryLbl.textAlignment = NSTextAlignmentRight;
    countryLbl.textColor = [UIColor darkGrayColor];
    countryLbl.font = [UIFont fontWithName:CTGeneralFont size:13.0];

    [cell addSubview:flagImg];
    [cell addSubview:countryLbl];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTShareInstance.countryIndex = indexPath.row;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
