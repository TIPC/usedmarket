//
//  CTSuperCell.m
//  usedMarket
//
//  Created by TIPC on 6/17/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTSuperCell.h"
#import "UIImageView+WebCache.h"
#import "Ads.h"
#import "Images.h"
#import <QuartzCore/QuartzCore.h>
@implementation CTSuperCell
@synthesize item = _item;

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setItem:(CTAdsModel *)newItem
{
    _item = newItem;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.adsImg.layer.masksToBounds = TRUE;
    self.adsImg.contentMode = UIViewContentModeScaleAspectFill;
    self.adsImg.clipsToBounds = YES;
    
    [self.adsImg setImageWithURL:[NSURL URLWithString:newItem.icon]];
    
    self.adsTitle.text = newItem.adsTitle;
    self.priceLb.text = newItem.price.stringValue;
    self.descriptionLbl.text = (self.parentVC.selectedIndex == self.tag)?newItem.descrp : @"";
    UIImage *img = (newItem.isInWishList)?[UIImage imageNamed:@"fullHeart"] : [UIImage imageNamed:@"emptyHeart"];
    [self.favBtn setImage:img forState:UIControlStateApplication];
    [self.favBtn setImage:img forState:UIControlStateNormal];
    
    self.adsTitle.layer.shadowColor = [[UIColor darkGrayColor] CGColor];
    self.adsTitle.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    
    self.adsTitle.layer.shadowRadius = 3.0;
    self.adsTitle.layer.shadowOpacity = 1.0;
    
        self.adsTitle.layer.masksToBounds = NO;
    
    
    self.adsTitle.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];

}


-(IBAction)addToWishList
{
    if (self.item.isInWishList)
    {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Ads"
                                              inManagedObjectContext:CTShareInstance.context];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(adsId == %@)",[NSString stringWithFormat:@"%@",self.item.adsId]]];
        [fetchRequest setEntity:entity];
        NSArray *existsRecord = [CTShareInstance.context executeFetchRequest:fetchRequest error:nil];
        for (NSManagedObject *tempItem in existsRecord)
            [CTShareInstance.context deleteObject:tempItem];
        self.item.isInWishList = NO;
    }
    else
    {
        Ads *tempAds = [NSEntityDescription insertNewObjectForEntityForName:@"Ads"
                                                     inManagedObjectContext:CTShareInstance.context];
        tempAds.adsId = [NSString stringWithFormat:@"%@",self.item.adsId];
        tempAds.publishDate = self.item.publishDate;
        for (NSString *tempImgUrl in self.item.imageList)
        {
            Images *tempImg = [NSEntityDescription insertNewObjectForEntityForName:@"Images" inManagedObjectContext:CTShareInstance.context];
            tempImg.url = tempImgUrl;
            tempImg.adsItem = tempAds;
            [tempAds addImageListObject:tempImg];
        }
        
        tempAds.adsTitle = self.item.adsTitle;
        tempAds.descrp = self.item.descrp;
        tempAds.price = self.item.price;
        

        self.item.isInWishList = YES;
    }
    [CTShareInstance.context save:nil];
    
    UIImage *img = (self.item.isInWishList)?[UIImage imageNamed:@"fullHeart"] : [UIImage imageNamed:@"emptyHeart"];
    [self.favBtn setImage:img forState:UIControlStateApplication];
    [self.favBtn setImage:img forState:UIControlStateNormal];
    
}
-(IBAction)showDetails
{

    if (self.parentVC.selectedIndex == self.tag)
    return;
    
    [self.parentVC.tableView beginUpdates];
    
    [self.parentVC.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.parentVC.selectedIndex inSection:0]]
                     withRowAnimation:UITableViewRowAnimationFade];
    NSInteger oldTab = self.parentVC.selectedIndex;
    self.parentVC.selectedIndex = self.tag;
    
    [self.parentVC.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:oldTab inSection:0]]
                     withRowAnimation:UITableViewRowAnimationFade];
    
    [self.parentVC.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.parentVC.selectedIndex inSection:0]]
                     withRowAnimation:UITableViewRowAnimationFade];
    
    [self.parentVC.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.parentVC.selectedIndex inSection:0]]
                     withRowAnimation:UITableViewRowAnimationFade];
    
    [self.parentVC.tableView endUpdates];

    [self.parentVC.tableView reloadData];
}
@end
