//
//  CTSpecCell.h
//  usedMarket
//
//  Created by TIPC on 6/21/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTAddOrderTVC.h"

@interface CTSpecCell : UITableViewCell<UITextFieldDelegate>
@property (strong, nonatomic) CTAddOrderTVC *parentVC;

@property (strong, nonatomic) IBOutlet UILabel *titleLbl;
@property (strong, nonatomic) IBOutlet UITextField *valueTxt;
@property (strong, nonatomic) IBOutlet UIButton *deleteBtn;
@property (strong, nonatomic) CTSpecificationItem *specificItem;
-(IBAction)deleteItem:(id)sender;

@end
