//
//  CTSuperCell.h
//  usedMarket
//
//  Created by TIPC on 6/17/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTAdsListTVC.h"
@interface CTSuperCell : UITableViewCell
@property (strong, nonatomic) CTAdsModel *item;
@property (strong, nonatomic) CTAdsListTVC *parentVC;
//@property (nonatomic) NSInteger itemIndex;

@property (strong, nonatomic) IBOutlet UIImageView *adsImg;
@property (strong, nonatomic) IBOutlet UILabel *adsTitle;
@property (strong, nonatomic) IBOutlet UILabel *priceLb;
@property (strong, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (strong, nonatomic) IBOutlet UIButton *favBtn;

-(IBAction)addToWishList;
-(IBAction)showDetails;

@end
