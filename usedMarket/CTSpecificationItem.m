//
//  CTSpecificationItem.m
//  usedMarket
//
//  Created by TIPC on 6/22/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTSpecificationItem.h"

@implementation CTSpecificationItem
@synthesize label,value,specId,keyboardType;
+(CTSpecificationItem *)specificationWithLabel:(NSString *)lbl forId:(NSString *)specID
{
    return [self specificationWithLabel:lbl forId:specID keyboard:UIKeyboardTypeDefault];

}
+(CTSpecificationItem *)specificationWithLabel:(NSString *)lbl forId:(NSString *)specID keyboard:(NSInteger)keyType
{
    CTSpecificationItem *tempSpec = [CTSpecificationItem new];
    tempSpec.label = lbl;
    tempSpec.specId = specID;
    tempSpec.value = @"";
    tempSpec.keyboardType = keyType;
    return tempSpec;

}
+(CTSpecificationItem *)specificationFromSpec:(CTSpecificationItem *)oldSpec
{
    CTSpecificationItem *tempSpec = [CTSpecificationItem new];
    tempSpec.label = oldSpec.label;
    tempSpec.specId = oldSpec.specId;
    tempSpec.value = oldSpec.value;
    tempSpec.keyboardType = oldSpec.keyboardType;
    return tempSpec;
}
-(NSString *)description
{
    return [NSString stringWithFormat:@"%@", @{@"label" : self.label, @"id" : specId, @"type" : @(self.keyboardType)}];
}

@end
