//
//  CTSuperViewController.m
//  usedMarket
//
//  Created by TIPC on 6/16/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTSuperViewController.h"

@interface CTSuperViewController ()

@end

@implementation CTSuperViewController


-(void)viewWillAppear:(BOOL)animated {
    
    // Set title
    self.navigationItem.title = titleView;
}

-(void)viewWillDisappear:(BOOL)animated {
    
    // Set title
    self.navigationItem.title = @"";
}


@end
