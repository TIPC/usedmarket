//
//  CTAddOrderTVC.m
//  usedMarket
//
//  Created by TIPC on 6/21/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTAddOrderTVC.h"
#import "CTSelectSpecificationTVC.h"
#import "CTSpecCell.h"
#import "RTSpinKitView.h"
@implementation CTAddOrderTVC
{
    iCarousel *carousel;
    CGFloat icarouselHeight;
    UIPageControl *pageController;
    NSArray *categoryList;
    UIView *topView;
    NSMutableArray *orderSpecification;
    NSArray *generalSpecification;
    NSInteger deleteIndex;
    NSInteger oldSelectCat;
    RTSpinKitView *loader;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIView *loaderBG = [UIView new];
    loaderBG.backgroundColor = [CTBackgroudColor colorWithAlphaComponent:0.7];
    loaderBG.frame = CGRectMake(0, 0,
                                self.view.frame.size.width,
                                self.view.frame.size.height);
    //[self.view addSubview:loaderBG];
    
    loader = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:CTNaviBlueColor];
    loader.hidesWhenStopped = YES;
    CGRect loaderFrame = loader.frame;
    loaderFrame.origin.x = (loaderBG.frame.size.width - loaderFrame.size.width)/2;
    loaderFrame.origin.y = (loaderBG.frame.size.height - loaderFrame.size.height)/2;
    loaderFrame.origin.y -= 50;
    loader.frame = loaderFrame;
    [loaderBG addSubview:loader];
    [loader stopAnimating];
    
    orderSpecification = [NSMutableArray new];
    generalSpecification = @[
                             [CTSpecificationItem specificationWithLabel:@"الحد الأدنى للسعر" forId:@"static1" keyboard:UIKeyboardTypeNumberPad],
                             [CTSpecificationItem specificationWithLabel:@"الحد الأعلى للسعر" forId:@"static2" keyboard:UIKeyboardTypeNumberPad]];
    [self.tableView addGestureRecognizer:[[UITapGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(touchBackground)]];

    topView = [UIView new];
    topView.backgroundColor = [UIColor whiteColor];
    
    self.view.backgroundColor = CTBackgroudColor;
    categoryList = CTShareInstance.categoryList;
    icarouselHeight = 140;
    
    UIBarButtonItem *sideMenu = [UIBarButtonItem new];
    sideMenu.image = [UIImage imageNamed:@"menu-icon"];
    [sideMenu setAction:@selector(presentRightMenuViewController:)];
    self.navigationItem.rightBarButtonItem = sideMenu;

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"حفظ"
                                                                             style:UIBarButtonItemStylePlain
                                                                            target:self
                                                                            action:@selector(saveData)];
    
    carousel = [iCarousel new];
    carousel.type = iCarouselTypeRotary;
    CGFloat frameTop = 50;
    carousel.frame = CGRectMake(0, frameTop, self.view.frame.size.width, icarouselHeight);
    carousel.delegate = self;
    carousel.dataSource = self;
    [topView addSubview:carousel];
    
    
    topView.frame = CGRectMake(0, 0, 320,
                               carousel.frame.size.height+carousel.frame.origin.y);
    [self.tableView addSubview:topView];
    [self.tableView registerNib:[UINib nibWithNibName:@"CTSpecCell" bundle:nil] forCellReuseIdentifier:@"specCell"];
    oldSelectCat = carousel.currentItemIndex;
    
    //[self.view bringSubviewToFront:loaderBG];
}
-(void)saveData
{
    BOOL emptyfields = NO;

    for (CTSpecificationItem *tempSpec in generalSpecification)
        if (tempSpec.value.length == 0) emptyfields = YES;
    
    for (CTSpecificationItem *tempSpec in orderSpecification)
        if (tempSpec.value.length == 0) emptyfields = YES;
    
    if (emptyfields)
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:@"الرجاء ملئ كافة الحقول"
                                   delegate:nil
                          cancelButtonTitle:@"موافق"
                          otherButtonTitles:nil] show];
    else
    {
        NSMutableArray *specList = [NSMutableArray new];
        for (CTSpecificationItem *tempSpec in generalSpecification)
            [specList addObject:@{@"specification" : @{
                                          @"id" : tempSpec.specId,
                                          @"label" : tempSpec.label,
                                          @"value" : tempSpec.value
                                          }}];
        
        for (CTSpecificationItem *tempSpec in orderSpecification)
            [specList addObject:@{@"specification" : @{
                                          @"id" : tempSpec.specId,
                                          @"label" : tempSpec.label,
                                          @"value" : tempSpec.value
                                          }}];
        
    }
}
-(void)touchBackground
{
    [self.view endEditing:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return categoryList.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UIView *catogeryCard = [UIView new];
    catogeryCard.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.95];
    catogeryCard.layer.cornerRadius = 11.0;
    catogeryCard.layer.masksToBounds = YES;
    catogeryCard.layer.borderColor = [UIColor lightGrayColor].CGColor;
    catogeryCard.layer.borderWidth = 1.0;
    catogeryCard.frame = CGRectMake(0, 0, 150, icarouselHeight);
    
    UIImageView *cardImg = [UIImageView new];
    cardImg.image = [UIImage imageNamed:categoryList[index][@"img"]];
    cardImg.frame = CGRectMake(0, -5, 150, 150);
    
    UILabel *cardLbl = [UILabel new];
    cardLbl.backgroundColor = [UIColor clearColor];
    cardLbl.text = categoryList[index][@"name_ar"];
    cardLbl.frame = CGRectMake(0, icarouselHeight - 50, catogeryCard.frame.size.width, 30);
    cardLbl.textColor = [UIColor darkGrayColor];
    cardLbl.font = [UIFont fontWithName:CTGeneralFont size:17.0];
    cardLbl.textAlignment = NSTextAlignmentCenter;
    cardLbl.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.9];
    
    [catogeryCard addSubview:cardImg];
    [catogeryCard addSubview:cardLbl];
    return catogeryCard;
}
- (void)carouselDidEndScrollingAnimation:(iCarousel *)_carousel
{
    if (oldSelectCat != carousel.currentItemIndex && orderSpecification.count > 0)
    {
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:@"هل أنت متأكد من أنك تريد تغيير التصنيف؟\nإن قيامك بالتأكد سيؤدي إلى إزالة المواصفات المضافة"
                                   delegate:self
                          cancelButtonTitle:@"إلغاء"
                          otherButtonTitles:@"تأكيد", nil] show];
    }
    else
        oldSelectCat = carousel.currentItemIndex;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
        [carousel scrollToItemAtIndex:oldSelectCat animated:YES];
    else
    {
        orderSpecification = [NSMutableArray new];
        [self.tableView reloadData];
    }
}

-(CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionSpacing:
            return value * 1.2f;
        case iCarouselOptionWrap:
            return YES;
        case iCarouselOptionFadeMin:
            return -0.2;
        case iCarouselOptionFadeMax:
            return 0.2;
        case iCarouselOptionFadeRange:
            return 2.0;
        default:
            return value;
    }
}

- (BOOL)carousel:(iCarousel *)_carousel shouldSelectItemAtIndex:(NSInteger)index
{
    return YES;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0)
        return generalSpecification.count+1;
    else if (section == 1)
        return orderSpecification.count;
    else if (section == 2)
        return 1;
    else
        return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)
        return topView.frame.size.height-20.0;
    else
        return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 0 && indexPath.row > 0) || (indexPath.section == 1))
    {
        CTSpecificationItem *record = (indexPath.section == 0)? generalSpecification[indexPath.row-1] : orderSpecification[indexPath.row];
        CTSpecCell *cell = [tableView dequeueReusableCellWithIdentifier:@"specCell"];
        cell.tag = indexPath.row;
        cell.parentVC = self;
        cell.specificItem = record;
        return cell;
    }
    else if (indexPath.section == 0 && indexPath.row == 0)
        return [UITableViewCell new];
    else if (indexPath.section == 2)
    {
        UITableViewCell *cell = [UITableViewCell new];
        UIButton *ddd = [UIButton new];
        ddd.backgroundColor = [UIColor redColor];
        ddd.frame = CGRectMake(3, 3, 30, 30);
        [ddd addTarget:self action:@selector(addSpecific) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:ddd];
        cell.textLabel.text = @"Add Specific";
        return cell;
    }
    return nil;

}
-(void)addSpecific
{
    if ([CTShareInstance.categoryList[carousel.currentItemIndex][@"specification"] count] == orderSpecification.count)
        [[[UIAlertView alloc] initWithTitle:@"" message:@"لقد قمت بإضافة كل المواصفات" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil] show];
        else
            [self.navigationController pushViewController:[[CTSelectSpecificationTVC alloc]
                                                           initWithParentList:orderSpecification forCategory:carousel.currentItemIndex]
                                                 animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < 2)
        return 30.0;
    else
        return 0.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *ss = [UIView new];
    ss.backgroundColor = CTBackgroudColor;
    ss.frame = CGRectMake(0, 0, 320, 30);
    
    UILabel *cc =  [UILabel new];
    cc.textAlignment = NSTextAlignmentRight;
    cc.frame = CGRectMake(10, 0, 300, 30);
    cc.textColor = [UIColor darkGrayColor];
    cc.font = [UIFont fontWithName:CTGeneralFont size:14.0];

    [ss addSubview:cc];
    if (section == 0)
        cc.text = @"إختر التصنيف وحدود السعر";
    else if (section == 1)
        cc.text = @"مواصفات السلعة";
    else
        cc.text = @"";

    return ss;

}
/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
        return @"إختر التصنيف وحدود السعر";
    else if (section == 1)
        return @"مواصفات السلعة";
    else
        return @"";
}
*/
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return (indexPath.section == 1);
}
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"حذف";
}
- (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [orderSpecification removeObjectAtIndex:indexPath.row];
    [self.tableView reloadData];
}

-(void)deleteSpecificationAtIndex:(NSInteger)index
{
    deleteIndex = index;
    [[[UIActionSheet alloc] initWithTitle:@"هل أنت متأكد من عملية الحذف"
                                delegate:self
                       cancelButtonTitle:@"إلغاء"
                  destructiveButtonTitle:@"حذف"
                        otherButtonTitles:nil] showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [orderSpecification removeObjectAtIndex:deleteIndex];
        [self.tableView reloadData];
    }
}
@end
