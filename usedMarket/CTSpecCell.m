//
//  CTSpecCell.m
//  usedMarket
//
//  Created by TIPC on 6/21/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTSpecCell.h"

@implementation CTSpecCell
@synthesize specificItem = _specificItem, parentVC;
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSpecificItem:(CTSpecificationItem *)newSpecificItem
{
    _specificItem = newSpecificItem;
    UIFont *font = [UIFont fontWithName:CTGeneralFont size:13.0];
    CGSize textSize = [newSpecificItem.label sizeWithAttributes:@{NSFontAttributeName:font}];
    CGRect lblFrame = self.titleLbl.frame;
    lblFrame.size.width = textSize.width;
    lblFrame.origin.x = self.frame.size.width - (lblFrame.size.width+10);
    self.titleLbl.frame = lblFrame;
    self.titleLbl.backgroundColor = [UIColor clearColor];
    self.titleLbl.font = font;
    self.titleLbl.text = newSpecificItem.label;
    self.titleLbl.textColor = self.valueTxt.textColor = [UIColor darkGrayColor];
    BOOL isStatic = ([newSpecificItem.specId rangeOfString:@"static"].location != NSNotFound);

    CGRect textFieldFrame = self.valueTxt.frame;
    textFieldFrame.size.width = self.frame.size.width - (lblFrame.size.width+60);
    textFieldFrame.origin.x = 40.0;
    self.deleteBtn.hidden = NO;
    if (isStatic)
    {
        textFieldFrame.size.width += 35;
        textFieldFrame.origin.x -= 35;
        self.deleteBtn.hidden = YES;
    }
    
    self.valueTxt.frame = textFieldFrame;
    self.valueTxt.keyboardType = newSpecificItem.keyboardType;
    self.valueTxt.backgroundColor = [UIColor clearColor];
    self.valueTxt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.valueTxt.layer.borderWidth = 1.0;
    self.valueTxt.delegate = self;
    self.deleteBtn.layer.cornerRadius = 15.0;
    self.valueTxt.text = self.specificItem.value;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.specificItem.value = self.valueTxt.text;
}
-(IBAction)deleteItem:(id)sender
{
    [self.parentVC deleteSpecificationAtIndex:self.tag];
}
@end
