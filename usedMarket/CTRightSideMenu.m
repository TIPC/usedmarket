//
//  CTRightSideMenu.m
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTRightSideMenu.h"

@implementation CTRightSideMenu
{
    NSArray *tableData;
    CGFloat cellHeight;
}
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableData = @[
                 @{@"title" : @"السوق", @"icon" : @"sideMenuMarket"},
                 @{@"title" : @"بحث", @"icon" : @"sideMenuSearch"},
                 @{@"title" : @"إضافة عرض", @"icon" : @"sideMenuOffer"},
                 @{@"title" : @"إضافة طلب", @"icon" : @"sideMenuOrder"},
                 @{@"title" : @"التنبيهات", @"icon" : @"sideMenuNoti"},
                 @{@"title" : @"المفضلة", @"icon" : @"sideMenuWishList"},
                 @{@"title" : @"الإعدادات", @"icon" : @"sideMenuSetting"}
                 ];
    cellHeight = 54.0;
    
    self.tableView = [UITableView new];
    self.tableView.frame =  CGRectMake(0, (self.view.frame.size.height - cellHeight * tableData.count) / 2.0f, self.view.frame.size.width, cellHeight * tableData.count);

    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    
    [self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *viewListId = @[@"market", @"search", @"addOffer", @"addRequest", @"notification", @"wishList", @"settings"];

    UIViewController *newContentView = CTShareInstance.appViews[viewListId[indexPath.row]];
    if (newContentView != self.sideMenuViewController.contentViewController)
        [self.sideMenuViewController setContentViewController:newContentView animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    UITableViewCell *cell = [UITableViewCell new];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:CTGeneralFont size:16.0];
    cell.textLabel.textColor = [UIColor whiteColor];
    

    cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
        cell.selectedBackgroundView = [[UIView alloc] init];
    
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.textLabel.text = [NSString stringWithFormat:@" %@", tableData[indexPath.row][@"title"]];
    UIImage *image = [UIImage imageNamed:tableData[indexPath.row][@"icon"]];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    CGRect imageFrame = imageView.frame;
    imageFrame.size = CGSizeMake(30, 30);
    imageView.frame =imageFrame;
    cell.accessoryView = imageView;

    return cell;
}

@end
