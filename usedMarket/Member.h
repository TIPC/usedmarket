//
//  Member.h
//  usedMarket
//
//  Created by TIPC on 8/25/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Member : NSObject
@property (strong,nonatomic) NSString *address;
@property (strong,nonatomic) NSString *countryId;
@property (strong,nonatomic) NSString *email;
@property (strong,nonatomic) NSString *mobile;
@property (strong,nonatomic) NSString *status;
@property (strong,nonatomic) NSString *userId;
@property (strong,nonatomic) NSString *userName;
@property (strong,nonatomic) NSString *passWord;






@end
