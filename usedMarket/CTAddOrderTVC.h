//
//  CTAddOrderTVC.h
//  usedMarket
//
//  Created by TIPC on 6/21/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface CTAddOrderTVC : UITableViewController<iCarouselDataSource, iCarouselDelegate, UIActionSheetDelegate, UIAlertViewDelegate>
-(void)deleteSpecificationAtIndex:(NSInteger)index;
@end
