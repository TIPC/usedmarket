//
//  CTSelectSpecificationTVC.m
//  usedMarket
//
//  Created by TIPC on 6/22/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTSelectSpecificationTVC.h"

@interface CTSelectSpecificationTVC ()

@end

@implementation CTSelectSpecificationTVC
{
    NSMutableArray *parentArray;
    NSMutableArray *specificationList;
}
-(id)initWithParentList:(NSMutableArray *)parentList forCategory:(NSInteger)catIndex;
{
    self = [self init];
    if (self)
    {
        parentArray = parentList;
        specificationList = [NSMutableArray new];
        BOOL noFound;
        for (CTSpecificationItem *tempSpec in CTShareInstance.categoryList[catIndex][@"specification"])
        {
            noFound = YES;
            for (CTSpecificationItem *tempSpec2 in parentArray)
                if ([tempSpec.specId isEqualToString:tempSpec2.specId])
                    noFound = NO;
            if (noFound)
                [specificationList addObject:tempSpec];
        }
            
    }
    return self;
}

#pragma mark - Table view data source

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationItem.title = @"قائمة المواصفات";
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return specificationList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTSpecificationItem *record = specificationList[indexPath.row];
    UITableViewCell *cell = [UITableViewCell new];
    
    
    UILabel *countryLbl = [UILabel new];
    countryLbl.backgroundColor = [UIColor clearColor];
    countryLbl.text = record.label;
    countryLbl.frame = CGRectMake(30.0, 2.0, 280.0, 40.0);
    countryLbl.textAlignment = NSTextAlignmentRight;
    countryLbl.textColor = [UIColor darkGrayColor];
    countryLbl.font = [UIFont fontWithName:CTGeneralFont size:14.0];
    
    [cell addSubview:countryLbl];
    

    
    // Configure the cell...
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTSpecificationItem *record = specificationList[indexPath.row];
    [parentArray addObject:[CTSpecificationItem specificationFromSpec:record]];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
