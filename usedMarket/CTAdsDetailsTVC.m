//
//  CTAdsDetailsTVC.m
//  usedMarket
//
//  Created by TIPC on 6/28/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTAdsDetailsTVC.h"
#import "UIImageView+WebCache.h"
#import "RTSpinKitView.h"
#import "Ads.h"
#import "Images.h"
@interface CTAdsDetailsTVC ()

@end

@implementation CTAdsDetailsTVC
{
    NSArray *imageList;
    NSArray *properties;
    NSMutableArray *tableData;
    UIScrollView *imageSwipe;

    RTSpinKitView *loader;
    BOOL isLoading;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:[UIImage imageNamed:@"sideMenuIcon"] forState:UIControlStateNormal];
    button.bounds = CGRectMake(0,0,button.imageView.image.size.width, button.imageView.image.size.height);
    [button addTarget:self action:@selector(presentRightMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"dd MM YYYY HH:MM"];
    tableData = [NSMutableArray new];
    [tableData addObject:@{@"name" : @"الوصف", @"value" : self.item.descrp}];
    [tableData addObject:@{@"name" : @"السعر", @"value" : self.item.price}];
    [tableData addObject:@{@"name" : @"تاريخ العرض", @"value" : [dateFormatter stringFromDate:self.item.publishDate]}];
    
    imageSwipe = [UIScrollView new];
    imageSwipe.frame = CGRectMake(0, 0, 320, 200);
    imageSwipe.pagingEnabled = YES;
    imageSwipe.showsHorizontalScrollIndicator = NO;
    imageSwipe.showsVerticalScrollIndicator = NO;
    [self.view addSubview:imageSwipe];
    
    loader = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:CTNaviBlueColor];
    loader.hidesWhenStopped = YES;
    CGRect loaderFrame = loader.frame;
    loaderFrame.origin.x = (self.view.frame.size.width - loaderFrame.size.width)/2;
    loaderFrame.origin.y = (self.view.frame.size.height - loaderFrame.size.height)/2;
    loaderFrame.origin.y -= 65;
    loader.frame = loaderFrame;
    [loader startAnimating];
    
    self.navigationItem.title = CTShareInstance.categoryList[CTShareInstance.categoryIndex][@"name_ar"];

    [self.view addSubview:loader];
    [self getDataFromWebService];
}

-(void)getDataFromWebService
{
    
    isLoading = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    CTShareInstance.request.sendData = @{@"item_id" : self.item.adsId};
    [CTShareInstance.request sendRequest:@"items/get_item_details"
                       completionHandler:^(NSDictionary *recivedData)
     {
         [loader stopAnimating];
         if ([recivedData[@"status"] boolValue] ||
             [recivedData[@"status"] isEqualToString:@"success"])
         {
             self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
             isLoading = NO;
              UIImageView *imageItem;
              NSInteger index = 0;
             imageList = recivedData[@"data"][@"img_list"];
              for (NSDictionary *imgData in imageList)
              {
                  imageItem = [UIImageView new];
                  imageItem.frame = CGRectMake(index*320, 0, 320, imageSwipe.frame.size.height);
                  imageItem.backgroundColor = UIColor.whiteColor;
                  imageItem.layer.masksToBounds = YES;
                  imageItem.contentMode = UIViewContentModeScaleAspectFit;
                  imageItem.clipsToBounds = YES;
                  [imageItem setImageWithURL:[NSURL URLWithString:imgData[@"link"]]];
                  [imageSwipe addSubview:imageItem];
                  ++index;
              }
              imageSwipe.contentSize = CGSizeMake(imageSwipe.frame.size.width*imageList.count,
                                                  imageSwipe.frame.size.height);
             properties = recivedData[@"data"][@"properties"];
             [self.tableView reloadData];

         }
         else
         {
             [[[UIAlertView alloc] initWithTitle:@""
                                         message:@"حدث خطأ أثناء الإتصال"
                                        delegate:nil
                               cancelButtonTitle:@"موافق"
                               otherButtonTitles:nil] show];
         }
         
         
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isLoading)
        return 0;
    else if (section == 0)
        return tableData.count+2;
    else if (section == 1)
        return properties.count;
    else
        return 2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0)
        return imageSwipe.frame.size.height;
    else if (indexPath.section == 0 && indexPath.row == 1)
        return [self getHeightForText:self.item.adsTitle
                            WithWidth:(self.view.frame.size.width - 20)
                                 Font:[UIFont fontWithName:CTGeneralFont size:15.0]];
    else if (indexPath.section == 0 && indexPath.row > 1)
        return [self getHeightForText:tableData[indexPath.row-2][@"value"]
                            WithWidth:230
                                 Font:[UIFont fontWithName:CTGeneralFont size:13.0]];
    else if (indexPath.section == 1)
        return [self getHeightForText:properties[indexPath.row][@"value"]
                            WithWidth:230
                                 Font:[UIFont fontWithName:CTGeneralFont size:13.0]];
    else
        return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [UITableViewCell new];
    if (indexPath.section == 0 && indexPath.row < 2)
    {
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont fontWithName:CTGeneralFont size:15.0];
        cell.textLabel.textAlignment = NSTextAlignmentRight;
        cell.textLabel.text =  (indexPath.row == 1)?self.item.adsTitle : @"";
        

    }
    else if (indexPath.section < 2)
    {
        UILabel *keyLbl = [UILabel new];
        UILabel *valueLbl = [UILabel new];
    
    keyLbl.frame = CGRectMake(240, 0, 80,  44);
    valueLbl.frame = CGRectMake(5, 0, 230, [self tableView:tableView heightForRowAtIndexPath:indexPath]);
    keyLbl.font = valueLbl.font = [UIFont fontWithName:CTGeneralFont size:13.0];
    
    keyLbl.textAlignment = NSTextAlignmentLeft;
    valueLbl.textAlignment = NSTextAlignmentRight;
    
    valueLbl.lineBreakMode = NSLineBreakByWordWrapping;
    valueLbl.numberOfLines = 0;

    keyLbl.textColor = CTNaviBlueColor;
    valueLbl.textColor = [UIColor darkGrayColor];
    if (indexPath.section == 0)
        keyLbl.text = tableData[indexPath.row-2][@"name"];
    else
        keyLbl.text = [NSString stringWithFormat:@"%@",properties[indexPath.row][@"name"]];
    
    if (indexPath.section == 0)
        valueLbl.text = [NSString stringWithFormat:@"%@",tableData[indexPath.row-2][@"value"]];
    else
        valueLbl.text = [NSString stringWithFormat:@"%@",properties[indexPath.row][@"value"]];
    
    [cell addSubview:keyLbl];
    [cell addSubview:valueLbl];
    }
    else if (indexPath.section == 2)
    {
        cell.textLabel.textColor = CTNaviBlueColor;
        cell.textLabel.font = [UIFont fontWithName:CTGeneralFont size:15.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        switch (indexPath.row)
        {
            case 0: cell.textLabel.text =  @"إتصل بالمعلن"; break;
            case 1: cell.textLabel.text =  (self.item.isInWishList)? @"أزل من قائمة الأماني" : @"أضف لقائمة الأماني"; break;
        }
        

    }
    return cell;
}

-(CGFloat)getHeightForText:(NSString *)valueText WithWidth:(CGFloat)labelWidth Font:(UIFont *)valueFont
{
    float expectedLabelSizeHeight;
    CGSize maximumLabelSize = CGSizeMake(labelWidth,9999);
    valueText = [NSString stringWithFormat:@"%@",valueText];
    expectedLabelSizeHeight = [valueText  boundingRectWithSize:maximumLabelSize
                                                     options:NSStringDrawingUsesLineFragmentOrigin
                                                  attributes:@{NSFontAttributeName:valueFont}
                                                     context:nil].size.height;
    
    if (expectedLabelSizeHeight < 34)
        expectedLabelSizeHeight = 34;
    
    return expectedLabelSizeHeight+10;
}

-(IBAction)addToWishList
{
    if (self.item.isInWishList)
    {
        NSFetchRequest *fetchRequest = [NSFetchRequest new];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Ads"
                                                  inManagedObjectContext:CTShareInstance.context];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(adsId == %@)",[NSString stringWithFormat:@"%@",self.item.adsId]]];
        [fetchRequest setEntity:entity];
        NSArray *existsRecord = [CTShareInstance.context executeFetchRequest:fetchRequest error:nil];
        for (NSManagedObject *tempItem in existsRecord)
            [CTShareInstance.context deleteObject:tempItem];
        self.item.isInWishList = NO;
    }
    else
    {
        Ads *tempAds = [NSEntityDescription insertNewObjectForEntityForName:@"Ads"
                                                     inManagedObjectContext:CTShareInstance.context];
        tempAds.adsId = [NSString stringWithFormat:@"%@",self.item.adsId];
        tempAds.publishDate = self.item.publishDate;
        for (NSString *tempImgUrl in self.item.imageList)
        {
            Images *tempImg = [NSEntityDescription insertNewObjectForEntityForName:@"Images" inManagedObjectContext:CTShareInstance.context];
            tempImg.url = tempImgUrl;
            tempImg.adsItem = tempAds;
            [tempAds addImageListObject:tempImg];
        }
        
        tempAds.adsTitle = self.item.adsTitle;
        tempAds.descrp = self.item.descrp;
        tempAds.price = self.item.price;
        
        
        self.item.isInWishList = YES;
    }
    [CTShareInstance.context save:nil];
    [self.tableView reloadData];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 2 && indexPath.row == 0)
    {
        [CTShareInstance checkLoggedUserWithAlert];
        [self.tableView reloadData];
    }
    else if (indexPath.section == 2 && indexPath.row == 1)
        [self addToWishList];
}

@end
