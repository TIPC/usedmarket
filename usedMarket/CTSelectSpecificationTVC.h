//
//  CTSelectSpecificationTVC.h
//  usedMarket
//
//  Created by TIPC on 6/22/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CTSelectSpecificationTVC : UITableViewController
-(id)initWithParentList:(NSMutableArray *)parentList forCategory:(NSInteger)catIndex;

@end
