//
//  Specification.m
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "Specification.h"
#import "Ads.h"


@implementation Specification

@dynamic pId;
@dynamic label;
@dynamic value;
@dynamic adsItem;

@end
