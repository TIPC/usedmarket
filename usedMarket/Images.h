//
//  Images.h
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ads;

@interface Images : NSManagedObject

@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) Ads *adsItem;

@end
