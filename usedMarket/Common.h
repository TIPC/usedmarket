//
//  Common.h
//  CleaningService
//
//  Created by Ibraheem Qanah on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WSRequest.h"
#import "CTAdsModel.h"
@class TBSuperAltibbiVC;
@class Member;

@interface Common : NSObject<UIAlertViewDelegate>

@property (strong, nonatomic) TBSuperAltibbiVC *topVC;

@property (strong, nonatomic) Member *loggedUser;
@property (strong, nonatomic) WSRequest *request;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) NSDictionary *appViews;
@property (nonatomic) NSInteger countryIndex;
@property (nonatomic) NSInteger categoryIndex;
@property (strong, nonatomic) NSString *docsDir;

+(id)sharedInstance;
-(NSString *)selectedCatId;
-(CTAdsModel *)prepareItem:(NSDictionary *)itemData;
-(NSString *)memberId;
-(NSArray *)countryList;
-(NSArray *)categoryList;
-(BOOL)checkLoggedUserWithAlert;
@end
