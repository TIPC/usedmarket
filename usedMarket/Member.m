//
//  Member.m
//  usedMarket
//
//  Created by TIPC on 8/25/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "Member.h"

@implementation Member
@synthesize address,countryId, email, mobile, status, userId, userName, passWord;
- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.address forKey:@"address"];
    [encoder encodeObject:self.countryId forKey:@"countryId"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.mobile forKey:@"mobile"];
    [encoder encodeObject:self.status forKey:@"status"];
    [encoder encodeObject:self.userId forKey:@"userId"];
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.passWord forKey:@"passWord"];

}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.address = [decoder decodeObjectForKey:@"address"];
        self.countryId = [decoder decodeObjectForKey:@"countryId"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.mobile = [decoder decodeObjectForKey:@"mobile"];
        self.status = [decoder decodeObjectForKey:@"status"];
        self.userId = [decoder decodeObjectForKey:@"userId"];
        self.userName = [decoder decodeObjectForKey:@"userName"];
        self.passWord = [decoder decodeObjectForKey:@"passWord"];
    }
    return self;
}

-(void)saveMember
{
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self];
    [[NSUserDefaults standardUserDefaults] setObject:encodedObject forKey:@"loggedUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
@end
