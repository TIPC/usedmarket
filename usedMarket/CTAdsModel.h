//
//  CTAdsModel.h
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTAdsModel : NSObject

@property (nonatomic, strong) NSString * link;
@property (nonatomic, strong) NSString * categoryId;
@property (nonatomic, strong) NSString * adsId;
@property (nonatomic, strong) NSString * adsTitle;
@property (nonatomic, strong) NSDate * publishDate;
@property (nonatomic, strong) NSNumber * adsType;
@property (nonatomic, strong) NSString * descrp;
@property (nonatomic, strong) NSNumber * price;
@property (nonatomic, strong) NSString * priceType;
@property (nonatomic, strong) NSNumber * countryId;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * ownerId;
@property (nonatomic, strong) NSString * ownerName;
@property (nonatomic, strong) NSString * ownerEmail;
@property (nonatomic, strong) NSString * ownerMobile;
@property (nonatomic, strong) NSArray *imageList;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSDictionary *specifications;
@property (nonatomic) BOOL isInWishList;

@end