//
//  Common.m
//  CleaningService
//
//  Created by Ibraheem Qanah on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Common.h"
#import "Member.h"
#import "Images.h"

#import "CTCategoriesListVC.h"
#import "CTAddOrderTVC.h"
#import "CTLoginTVC.h"
#import "RESideMenu.h"
@implementation Common

@synthesize loggedUser, request, context, docsDir, countryIndex, appViews;
-(NSArray *)countryList
{
    return @[
             @{@"name" : @"الأردن", @"icon" : @"JO", @"id" : @"1"},
             @{@"name" : @"الصومال", @"icon" : @"SO", @"id" : @"2"},
             @{@"name" : @"الإمارات", @"icon" : @"AE", @"id" : @"3"},
             @{@"name" : @"البحرين", @"icon" : @"BH", @"id" : @"4"},
             @{@"name" : @"تونس", @"icon" : @"TN", @"id" : @"5"},
             @{@"name" : @"الجزائر", @"icon" : @"DZ", @"id" : @"6"},
             /*@{@"name" : @"جيبوتي", @"icon" : @""},*/
             @{@"name" : @"المملكة العربية السعودية", @"icon" : @"SA", @"id" : @"7"},
             @{@"name" : @"السودان", @"icon" : @"SD", @"id" : @"8"},
             @{@"name" : @"سوريا", @"icon" : @"SY", @"id" : @"9"},
             @{@"name" : @"العراق", @"icon" : @"IQ", @"id" : @"10"},
             @{@"name" : @"سلطنة عمان", @"icon" : @"OM", @"id" : @"11"},
             @{@"name" : @"فلسطين", @"icon" : @"PS", @"id" : @"12"},
             @{@"name" : @"قطر", @"icon" : @"QA", @"id" : @"13"},
             @{@"name" : @"لبنان", @"icon" : @"LB", @"id" : @"14"},
             @{@"name" : @"ليبيا", @"icon" : @"LY", @"id" : @"15"},
             @{@"name" : @"مصر", @"icon" : @"EG", @"id" : @"16"},
             @{@"name" : @"المغرب", @"icon" : @"MA", @"id" : @"17"},
             @{@"name" : @"موريتانيا", @"icon" : @"MR", @"id" : @"18"},
             @{@"name" : @"اليمن", @"icon" : @"YE", @"id" : @"19"}
             ];
}
+ (id)sharedInstance
{
    static Common *sharedInstance = nil;
    @synchronized(self)
    {
        if (sharedInstance == nil)
            sharedInstance = [self new];
    }
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    
    NSData *encodedObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedUser"];
    self.loggedUser = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    
    self.request = [WSRequest new];
    self.docsDir = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    
    self.countryIndex = 6;
    
    self.appViews = @{
                      @"market" : [[UINavigationController alloc] initWithRootViewController:[CTCategoriesListVC new]],
                      @"search" : [[UINavigationController alloc] initWithRootViewController:[UIViewController new]],
                      @"addOffer"  : [[UINavigationController alloc] initWithRootViewController:[UIViewController new]],
                      @"addRequest" : [[UINavigationController alloc] initWithRootViewController:[[CTAddOrderTVC alloc] initWithStyle:UITableViewStyleGrouped]],
                      @"notification" : [[UINavigationController alloc] initWithRootViewController:[UIViewController new]],
                      @"wishList" : [[UINavigationController alloc] initWithRootViewController:[UIViewController new]],
                      @"settings" : [[UINavigationController alloc] initWithRootViewController:[UIViewController new]]
                      };
    for (NSString *key in self.appViews)
    {
        UINavigationController *tempNaviVC = self.appViews[key];
        [tempNaviVC.viewControllers[0] setNeedsStatusBarAppearanceUpdate];
        [tempNaviVC.navigationBar setBarTintColor:CTNaviBlueColor];
        [tempNaviVC.navigationBar setTranslucent:YES];
        tempNaviVC.navigationBar.tintColor = [UIColor whiteColor];
        tempNaviVC.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"sideMenuIcon"] forState:UIControlStateNormal];
        button.bounds = CGRectMake(0,0,button.imageView.image.size.width, button.imageView.image.size.height);
        [button addTarget:tempNaviVC.viewControllers[0] action:@selector(presentRightMenuViewController:) forControlEvents:UIControlEventTouchUpInside];
        [tempNaviVC.viewControllers[0] navigationItem].rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];

        
    }
    return self;
    
}

-(NSString *)memberId
{
    if (self.loggedUser != nil)
        return self.loggedUser.userId;
    return @"0";
}

-(NSArray *)categoryList
{
    /*
     UIKeyboardTypeDefault,                // Default type for the current input method.
     UIKeyboardTypeASCIICapable,           // Displays a keyboard which can enter ASCII characters, non-ASCII keyboards remain active
     UIKeyboardTypeNumbersAndPunctuation,  // Numbers and assorted punctuation.
     UIKeyboardTypeURL,                    // A type optimized for URL entry (shows . / .com prominently).
     UIKeyboardTypeNumberPad,              // A number pad (0-9). Suitable for PIN entry.
     UIKeyboardTypePhonePad,               // A phone pad (1-9, *, 0, #, with letters under the numbers).
     UIKeyboardTypeNamePhonePad,           // A type optimized for entering a person's name or phone number.
     UIKeyboardTypeEmailAddress,           // A type optimized for multiple email address entry (shows space @ . prominently).
     UIKeyboardTypeDecimalPad NS_ENUM_AVAILABLE_IOS(4_1),   // A number pad with a decimal point.
     UIKeyboardTypeTwitter NS_ENUM_AVAILABLE_IOS(5_0),      // A type optimized for twitter text entry (easy access to @ #)
     UIKeyboardTypeWebSearch NS_ENUM_AVAILABLE_IOS(7_0),    // A default keyboard type with URL-oriented addition (shows space . prominently).
     
     UIKeyboardTypeAlphabet = UIKeyboardTypeASCIICapable, // Deprecated

     */

    return @[
             @{@"id" : @"1", @"name" : @"عقارات", @"img" : @"01",
               @"specification" : [NSMutableDictionary new]},

             @{@"id" : @"1", @"name" : @"سيارات", @"img" : @"02",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"هواتف", @"img" : @"03",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"خدمات", @"img" : @"04",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"وظائف", @"img" : @"01",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"أجهزة", @"img" : @"02",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"أثاث", @"img" : @"02",
               @"specification" : [NSMutableDictionary new]},
             
             @{@"id" : @"1", @"name" : @"اخرى", @"img" : @"02",
               @"specification" : [NSMutableDictionary new]}
             /*

             @{@"id" : @"5", @"name_ar" : @"", @"name_en" : @"", @"img" : @"01",
               @"specification" : @[
                       [CTSpecificationItem specificationWithLabel:@"شاشة لمس" forId:@"19"],
                       [CTSpecificationItem specificationWithLabel:@"البطارية" forId:@"20"],
                       [CTSpecificationItem specificationWithLabel:@"الكورة" forId:@"21"],
                       [CTSpecificationItem specificationWithLabel:@"إلخ" forId:@"22"]
                       ]
               },
             @{@"id" : @"6", @"name_ar" : @"", @"name_en" : @"", @"img" : @"02",
               @"specification" : @[
                       [CTSpecificationItem specificationWithLabel:@"شاشة لمس" forId:@"23"],
                       [CTSpecificationItem specificationWithLabel:@"البطارية" forId:@"24"],
                       [CTSpecificationItem specificationWithLabel:@"الكورة" forId:@"25"],
                       [CTSpecificationItem specificationWithLabel:@"إلخ" forId:@"26"]
                       ]
               },
             @{@"id" : @"7", @"name_ar" : @"", @"name_en" : @"", @"img" : @"03",
               @"specification" : @[
                       [CTSpecificationItem specificationWithLabel:@"شاشة لمس" forId:@"27"],
                       [CTSpecificationItem specificationWithLabel:@"البطارية" forId:@"28"],
                       [CTSpecificationItem specificationWithLabel:@"الكورة" forId:@"29"],
                       [CTSpecificationItem specificationWithLabel:@"إلخ" forId:@"30"]
                       ]
               },
             @{@"id" : @"8", @"name_ar" : @"", @"name_en" : @"", @"img" : @"04",
               @"specification" : @[
                       [CTSpecificationItem specificationWithLabel:@"شاشة لمس" forId:@"31"],
                       [CTSpecificationItem specificationWithLabel:@"البطارية" forId:@"32"],
                       [CTSpecificationItem specificationWithLabel:@"الكورة" forId:@"33"],
                       [CTSpecificationItem specificationWithLabel:@"إلخ" forId:@"34"]
                       ]
               }
              */
             ];
}
-(NSString *)selectedCatId
{
    return self.categoryList[self.categoryIndex][@"id"];
}
-(NSString *)filterStringFromNull:(NSString *)str
{
    return str;
}
-(CTAdsModel *)prepareItem:(NSDictionary *)itemData
{
    CTAdsModel *tempItem = [CTAdsModel new];
    NSTimeInterval publishDate = [self filterStringFromNull:itemData[@"create_time"]].doubleValue;
    
    tempItem.publishDate = [NSDate dateWithTimeIntervalSince1970:publishDate];
    tempItem.descrp = [self filterStringFromNull:itemData[@"description"]];
    tempItem.adsId = [self filterStringFromNull:itemData[@"id"]];
    tempItem.link = [self filterStringFromNull:itemData[@"link"]];
    tempItem.adsTitle = [self filterStringFromNull:itemData[@"name"]];
    tempItem.price = @([self filterStringFromNull:itemData[@"price"]].floatValue);
    
    
    tempItem.icon = [self filterStringFromNull:itemData[@"icon"]];
        
    NSFetchRequest *fetchRequest = [NSFetchRequest new];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Ads"
                                              inManagedObjectContext:CTShareInstance.context];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"(adsId == %@)",tempItem.adsId]];
    [fetchRequest setEntity:entity];
    NSArray *existsRecord = [CTShareInstance.context executeFetchRequest:fetchRequest error:nil];
    tempItem.isInWishList = (existsRecord.count > 0);
    return tempItem;
}

-(BOOL)checkLoggedUserWithAlert
{
    
    [[[UIAlertView alloc] initWithTitle:@""
                                message:@"الرجاء تسجيل الدخول لإتمام هذه العملية"
                               delegate:self
                      cancelButtonTitle:@"إلغاء"
                      otherButtonTitles:@"تسجيل دخول", nil]show];
    
    return NO;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        RESideMenu *menuView = (RESideMenu *)[[[UIApplication sharedApplication] windows][0] rootViewController];
        [((UINavigationController *)menuView.contentViewController) pushViewController:[CTLoginTVC new] animated:YES];
    }
}
@end

