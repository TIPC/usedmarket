//
//  CTCategoriesListVC.h
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "iCarousel.h"
#import "CTAdsListTVC.h"

@interface CTCategoriesListVC : CTSuperViewController<iCarouselDataSource, iCarouselDelegate>

@property (nonatomic, strong) IBOutlet iCarousel *carousel;

@end
