//
//  CTLoginTVC.m
//  usedMarket
//
//  Created by TIPC on 8/25/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTLoginTVC.h"
#import "RTSpinKitView.h"
@interface CTLoginTVC ()

@end

@implementation CTLoginTVC
{
    UITextField *username;
    UITextField *password;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"تسجيل دخول";
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                            action:@selector(hideKeyboard)]];
    
    
    self.navigationItem.title = CTShareInstance.categoryList[CTShareInstance.categoryIndex][@"name_ar"];
    

}
-(void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1)
        return 130;
    else
        return 0;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return section+2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [NSString stringWithFormat:@"Cell%d-%d",indexPath.section, indexPath.row];
    
    
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell)
        return cell;
    
    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    
    if (indexPath.section == 0)
    {
        UILabel *keyLbl = [[UILabel alloc] initWithFrame:CGRectMake(220, 0, 95,  44)];
        UITextField *valueLbl = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, 210,44)];
        
        keyLbl.font = valueLbl.font = [UIFont fontWithName:CTGeneralFont size:13.0];
        
        keyLbl.textAlignment = NSTextAlignmentLeft;
        valueLbl.textAlignment = NSTextAlignmentRight;
        
        keyLbl.textColor = CTNaviBlueColor;
        valueLbl.textColor = [UIColor darkGrayColor];
        
        keyLbl.text = (indexPath.row == 0)? @"إسم المستخدم" : @"كلمة المرور";
        if (indexPath.row == 0)
            username = valueLbl;
        else
            password = valueLbl;
        
        valueLbl.secureTextEntry = (indexPath.row == 1);

        valueLbl.delegate = self;
        
        valueLbl.returnKeyType = UIReturnKeyGo;
        [cell addSubview:keyLbl];
        [cell addSubview:valueLbl];
    }
    else
    {
        cell.textLabel.textColor = CTNaviBlueColor;
        cell.textLabel.font = [UIFont fontWithName:CTGeneralFont size:15.0];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        switch (indexPath.row)
        {
            case 0: cell.textLabel.text =  @"تسجيل دخول"; break;
            case 1: cell.textLabel.text =  @"حساب جديد"; break;
            case 2: cell.textLabel.text =  @"إسترداد كلمة المرور"; break;
        }

    }
    // Configure the cell...
 
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"DASDSA");
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
            [self login];
    }
}

-(void)login
{
    [self hideKeyboard];
    UIAlertView *loader = [[UIAlertView alloc] initWithTitle:@"الرجاء الإنتظار..." message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [loader show];
    CTShareInstance.request.sendData = @{@"login" : username.text,
                                         @"password" : password.text};
    [CTShareInstance.request sendRequest:@"auth/login"
                       completionHandler:^(NSDictionary *recivedData)
     {
         NSLog(@"%@",recivedData);
         [loader dismissWithClickedButtonIndex:0 animated:YES];

         if ([recivedData[@"status"] boolValue] ||
             [recivedData[@"status"] isEqualToString:@"success"])
         {
             /*
              
              data =     {
              address = "<null>";
              "country_id" = "<null>";
              email = "mostafa.balata@gmail.com";
              mobile = 9799002;
              status = 1;
              "user_id" = 5;
              username = mostafa;
              };

              */
             [[[UIAlertView alloc] initWithTitle:@""
                                         message:@"الرجاء التأكد من صحة البيانات المدخلة"
                                        delegate:nil
                               cancelButtonTitle:@"موافق"
                               otherButtonTitles:nil] show];
         }
         else
             [[[UIAlertView alloc] initWithTitle:@""
                                         message:@"الرجاء التأكد من صحة البيانات المدخلة"
                                        delegate:nil
                               cancelButtonTitle:@"موافق"
                               otherButtonTitles:nil] show];
         
         
     }];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self login];
    return YES;
}


@end
