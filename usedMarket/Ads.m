//
//  Ads.m
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "Ads.h"
#import "Images.h"
#import "Specification.h"


@implementation Ads

@dynamic link;
@dynamic categoryId;
@dynamic adsId;
@dynamic adsTitle;
@dynamic publishDate;
@dynamic adsType;
@dynamic descrp;
@dynamic price;
@dynamic priceType;
@dynamic countryId;
@dynamic city;
@dynamic address;
@dynamic ownerId;
@dynamic ownerName;
@dynamic ownerEmail;
@dynamic ownerMobile;
@dynamic imageList;
@dynamic specifications;
-(CTAdsModel *)getAdsModel
{
    CTAdsModel *tempModel = [CTAdsModel new];
    
    return tempModel;
}
@end
