//
//  CTAdsModel.m
//  usedMarket
//
//  Created by TIPC on 6/19/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTAdsModel.h"

@implementation CTAdsModel

@synthesize link;
@synthesize categoryId;
@synthesize adsId;
@synthesize adsTitle;
@synthesize publishDate;
@synthesize adsType;
@synthesize descrp;
@synthesize price;
@synthesize priceType;
@synthesize countryId;
@synthesize city;
@synthesize address;
@synthesize ownerId;
@synthesize ownerName;
@synthesize ownerEmail;
@synthesize ownerMobile;
@synthesize imageList;
@synthesize icon;

@synthesize specifications;

@end
