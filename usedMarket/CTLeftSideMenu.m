//
//  CTLeftSideMenu.m
//  usedMarket
//
//  Created by TIPC on 6/15/14.
//  Copyright (c) 2014 competitive. All rights reserved.
//

#import "CTLeftSideMenu.h"

@implementation CTLeftSideMenu

{
    NSArray *tableData;
    CGFloat cellHeight;
}
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    tableData = CTShareInstance.categoryList;
    cellHeight = 50.0;
    
    self.tableView = [UITableView new];
    self.tableView.frame =  CGRectMake(0, (self.view.frame.size.height - cellHeight * tableData.count) / 2.0f, self.view.frame.size.width, cellHeight * tableData.count);
    
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundView = nil;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.bounces = NO;
    
    [self.view addSubview:self.tableView];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            /*[self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[DEMOFirstViewController alloc] init]]
             animated:YES];
             [self.sideMenuViewController hideMenuViewController];
             break;
             case 1:
             [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:[[DEMOSecondViewController alloc] init]]
             animated:YES];
             [self.sideMenuViewController hideMenuViewController];*/
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return cellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [UITableViewCell new];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.font = [UIFont fontWithName:CTGeneralFont size:16.0];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    
    cell.textLabel.highlightedTextColor = [UIColor lightGrayColor];
    cell.selectedBackgroundView = [[UIView alloc] init];
    
    cell.textLabel.textAlignment = NSTextAlignmentLeft;
    cell.textLabel.text = [NSString stringWithFormat:@"  %@",tableData[indexPath.row][@"name_ar"]];

    
    return cell;
}

@end
